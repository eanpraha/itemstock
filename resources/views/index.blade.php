@extends('layout/main')

@section('title','itemstock')

@section('container')


<!-- index -->
<div class="container mt-3 mb-3 bg-light p-2 shadow-sm">
    <div class="row">
        <!-- main -->
        <div class="col-sm-12 col-md-12 col-xl-9">
            <div class="container">
                <div class="row">


                    <!-- post -->
                    @foreach($itemstock as $i)


                    <div class="col-sm-4 col-md-3 col-xl-3 mb-2" style="width: 12rem;">
                        <div class="card shadow h-100">
                            <img src="{{ url('img').('/').($i->gambar) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">
                                    <b><a href="{{ url('items').('/show').('/').($i->id) }}">{{ $i->nama }}</a></b>
                                </p>
                                <div class="card-footer">
                                    <small class="text-muted">harga beli:
                                        Rp. {{ number_format($i->hargabeli) }},-
                                    </small>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">harga jual:
                                        Rp. {{ number_format($i->hargajual) }},-
                                    </small>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">stok : {{ $i->persediaan }}
                                    </small>
                                </div>
                                <div class="card-footer pl-2">
                                    <div class="btn-group">
                                        <a class="btn btn-info btn-xs " href="{{ url('items').('/edit').('/').($i->id) }}" role="button">Edit</a>
                                        <button type="button" class="btn btn-danger btn-xs rounded-right" data-toggle="modal" data-target="#exampleModal">
                                            Hapus
                                        </button>

                                        <!-- modal hapus -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Item ?</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-info btn-xs" data-dismiss="modal">Cancel</button>
                                                        <a class="btn btn-danger btn-xs" href="{{ url('items').('/hapus').('/').($i->id) }}" role="button">Hapus</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <button class="btn btn-info btn-sm" type="button">Edit</button> -->
                                <!-- <button class="btn btn-danger btn-sm" type="button">Hapus</button> -->
                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>




                <!-- pagination -->
                <div class="container container-fluid align-center m-4">
                    <div class="row">
                        <div class="col-4">
                        </div>
                        <div class="col-4">
                            {{ $itemstock->links() }}
                        </div>
                    </div>
                </div>

                <!-- akhir pagination -->
                <!-- penanda -->
            </div>
        </div>
        <!-- akhir post -->

        <!-- widget kanan -->
        <!-- random item -->
        <div class="col-sm-12 col-md-12 col-xl-3">
            <div class="card shadow mb-3">
                <div class="card-body">
                    <h5 class="card-title">List item</h5>
                    <p class="card-text">
                        <ul>
                            @foreach($itemstock as $i)
                            <li>
                                <a href="{{ url('items').('/show').('/').($i->id) }}">{{ $i->nama }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </p>
                </div>
            </div>
            <!-- akhir random item -->
            <!-- info -->
            <table class="table mt-2 shadow">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Item</th>
                        <th scope="col">Stok</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($itemstock as $i)
                    <tr>
                        <td>{{ $i->nama }}</td>
                        <td>{{ $i->persediaan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <!-- akhir info -->
        </div>
        <!-- p -->

    </div>
</div>

<!-- akhir index -->


@endsection