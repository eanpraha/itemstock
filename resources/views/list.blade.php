@extends('layout/main')

@section('title','itemstock')

@section('container')



<!-- index -->
<div class="container mt-3 mb-3 bg-light p-2 shadow-sm">
    <div class="row">
        <!-- main -->
        <div class="col-9">
            <div class="row">
                <div class="container">

                    <!-- list -->
                    <table class="table mt-2 ml-3 shadow">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nama Item</th>
                                <th scope="col">Harga beli</th>
                                <th scope="col">Harga jual</th>
                                <th scope="col">Persediaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($itemstock as $i)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $i->nama }}</td>
                                <td>Rp. {{ number_format($i->hargabeli) }},-</td>
                                <td>Rp. {{ number_format($i->hargajual) }},-</td>
                                <td>{{ $i->persediaan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- penanda -->
            </div>
        </div>
        <!-- akhir post -->

        <!-- widget kanan -->
        <!-- random item -->
        <div class="col-3">

        </div>
        <!-- p -->

    </div>
</div>

<!-- akhir index -->


@endsection