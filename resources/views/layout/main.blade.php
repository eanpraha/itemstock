<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>@yield('title')</title>

    <style>
        .nav {
            font-family: 'Helvetica';
            height: 40px;
            border-bottom: 1px solid #333;

        }

        .nav-item {
            margin-right: 24px;
            font-weight: 200;

        }
    </style>
</head>

<body>
    <img src="/img/banner.png" class="img-fluid" alt="">

    <!-- navbar -->
    <nav class="navbar navbar-expand navbar-dark bg-dark sticky-top">
        <div class="nav navbar-nav navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-item nav-link active ml-5" href="{{url('/')}}">Home</a>
                </li>
                <a class="nav-item nav-link active ml-5" data-toggle="modal" data-target="#exampleModalCenter">Add Item</a>
                <li class="nav-item active">
                    <a class="nav-item nav-link active ml-5" href="{{url('/list')}}">List</a>
                </li>
                <a class="nav-item nav-link active ml-5" href="{{url('/about')}}">About</a>
                <li class="nav-item active">
                </li>
            </ul>
            <form action="/items/cari" method="GET" class="form-inline fluid mr-5">
                <input class="form-control  fluid" type="search" placeholder="Search" value="{{ old('cari') }}">
                <button class="btn btn-outline-light btn-sm ml-1" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <!-- akhir navbar -->


    <!-- Modal form  -->
    <div class="modal fade sha" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- form -->
                    <form action="/items/create" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            {{csrf_field()}}

                            <label>Name Barang</label>
                            <input type="text" name="nama" class="form-control rounded-0" placeholder="Nama Barang" required>
                        </div>
                        <div class="form-group">
                            <label>Harga Beli</label>
                            <div class="container">
                                <div class="row">
                                    <div class="col-1">
                                        Rp.
                                    </div>
                                    <div class="col-11">
                                        <input type="number" name="hargabeli" class="form-control rounded-0" placeholder="Harga beli" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Harga Jual</label>
                            <div class="container">
                                <div class="row">
                                    <div class="col-1">
                                        Rp.
                                    </div>
                                    <div class="col-11">
                                        <input type="number" name="hargajual" class="form-control rounded-0" placeholder="Harga jual" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Stok barang</label>
                            <input type="number" name="persediaan" class="form-control rounded-0" placeholder="Stok barang" required>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="gambar">
                                <label class="custom-file-label" for="image">Pilih foto</label>
                                <small class="form-text text-muted" id="fileHelp">Format foto JPG/PNG & Ukuran maksimal 100kb.</small>
                            </div>
                        </div>
                        <!-- akhir form-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add Item</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- akhir modal form -->

    <!-- alert -->
    <div class="container">
        @if(session('sukses'))
        <div class="alert alert-primary" role="alert">
            Item berhasil ditambahkan !
        </div>
        @endif

        @if(session('sukses2'))
        <div class="alert alert-primary" role="alert">
            Item berhasil diedit !
        </div>
        @endif

    </div>

    @yield('container')



    <!-- footer -->
    <nav class="navbar navbar-inverse navbar-fixed-bottom bg-dark">
        <div class="container text-light p-3">
            <div class="row">
                <div class="col-8 col-sm-12 col-md-6 col-lg-8">
                    <h2>Stock of Item</h2>
                    <hr class="bg-light">
                    <p>Itemstock is webproject created in <a href="http://www.laravel.com">laravel frameworks</a>.</p>
                </div>
            </div>
            <div class="col-4 col-sm-12 col-md-6 col-lg-4 align-self-end">
                <p>Powered by <a href="http://www.laravel.com">laravel</a> & <a href="http://www.getbootstrap.com">bootstrap</a>, design by eanpraha @2019</p>
            </div>
        </div>
    </nav>
    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>