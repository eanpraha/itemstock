@extends('layout/main')

@section('title','itemstock')

@section('container')

<!-- index -->
<div class="container mt-3 mb-3 bg-light p-2 shadow-sm">
    <div class="row">
        <!-- main -->
        <div class="col-9">
            <div class="row">
                <div class="container p-4 ml-4">
                    <div class="col-6">
                        <div class="card shadow h-100">
                            <img src="{{ url('img').('/').($item->gambar) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">
                                    <b><a href="{{ url('items').('/show').('/').($item->id) }}">{{$item->nama}}</a></b>
                                    <hr>
                                </p>
                                <div class="card-text">
                                    harga beli:
                                    Rp. {{ number_format($item->hargabeli) }},-

                                </div>
                                <div class="card-text">
                                    harga jual:
                                    Rp. {{ number_format($item->hargajual) }},-

                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">stok : {{ $item->persediaan }}
                                    </small>
                                </div>

                                <div class="btn-group m-5">
                                    <a class="btn btn-info btn-xs " href="{{ url('items').('/edit').('/').($item->id) }}" role="button">Edit</a>
                                    <button type="button" class="btn btn-danger btn-xs rounded-right" data-toggle="modal" data-target="#exampleModal">
                                        Hapus
                                    </button>

                                    <!-- modal hapus -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Item ?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-info btn-xs" data-dismiss="modal">Cancel</button>
                                                    <a class="btn btn-danger btn-xs" href="{{ url('items').('/hapus').('/').($item->id) }}" role="button">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <button class="btn btn-info btn-sm" type="button">Edit</button> -->
                                    <!-- <button class="btn btn-danger btn-sm" type="button">Hapus</button> -->
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- penanda -->
                </div>
            </div>
            <!-- akhir post -->

            <!-- widget kanan -->
            <!-- random item -->

            <!-- p -->

        </div>
    </div>

    <!-- akhir index -->


    @endsection