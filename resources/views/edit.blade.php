@extends('layout/main')

@section('title','itemstock')

@section('container')

<!-- index -->
<div class="container mt-3 mb-3 bg-light p-2 shadow-sm">
    <div class="row">
        <!-- main -->
        <div class="col-9">
            <div class="row">
                <div class="container p-4 ml-4">


                    <!-- Edit -->
                    <h4>Edit Item</h4>
                    <!-- form -->
                    <form action="/items/update/{$id}" method="POST">
                        <div class="form-group">
                            {{csrf_field()}}
                            <label>Name Barang</label>
                            <input type="text" name="nama" class="form-control rounded-0" placeholder="Nama Barang" value="{{$item->nama}}" required>
                        </div>
                        <div class="form-group">
                            <label>Harga Beli</label>
                            <div class="container">
                                <div class="row">
                                    <div class="col-1">
                                        Rp.
                                    </div>
                                    <div class="col-11">
                                        <input type="number" name="hargabeli" class="form-control rounded-0" placeholder="Harga beli" value="{{$item->hargabeli}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Harga Jual</label>
                            <div class="container">
                                <div class="row">
                                    <div class="col-1">
                                        Rp.
                                    </div>
                                    <div class="col-11">
                                        <input type="number" name="hargajual" class="form-control rounded-0" placeholder="Harga jual" value="{{$item->hargajual}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Stok barang</label>
                            <input type="number" name="persediaan" class="form-control rounded-0" placeholder="Stok barang" value="{{$item->persediaan}}" required>
                        </div>

                        <img src="{{ url('img').('/').($item->gambar) }}" class="img-thumbnail" style="width: 12rem; height: 12rem;">

                        <div class="form-group">
                            <label>Foto</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="gambar">
                                <label class="custom-file-label" for="image">Pilih foto</label>
                                <small class="form-text text-muted" id="fileHelp">Format foto JPG/PNG & Ukuran maksimal 100kb.</small>
                            </div>
                        </div>
                        <!-- akhir form-->
                        <button type="submit" class="btn btn-primary float-right">Edit Item</button>
                </div>
                </form>

            </div>
        </div>

        <!-- penanda -->
    </div>
</div>
<!-- akhir post -->

<!-- widget kanan -->
<!-- random item -->
<div class="col-3">

    <!-- akhir info -->
</div>
<!-- p -->

</div>
</div>

<!-- akhir index -->


@endsection