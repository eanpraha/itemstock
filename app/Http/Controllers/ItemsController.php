<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Item;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ItemsController extends Controller
{
    public function index()
    {
        $item = Item::all();
        $item = DB::table('itemstock')->Paginate(8);

        return view('index', ['itemstock' => $item]);
    }

    public function cari(Request $request)
    {
        $cari = $request->cari;

        $item = DB::table('itemstock')
            ->where('nama', 'like', "%" . $cari . "%")
            ->paginate();

        return view('index', ['item' => $item]);
    }

    public function hapus($id)
    {
        DB::table('itemstock')->where('id', $id)->delete();

        return redirect('/');
    }

    public function list()
    {
        $item = Item::all();
        return view('list', ['itemstock' => $item]);
    }


    public function create(Request $request)
    {
        //\App\Item::create($request->all());

        $this->validate($request, [
            'nama' => 'required',
            'gambar' => 'required|file|image|mimes:jpeg,jpg,png|max:100'
        ]);

        //menyimpan data file yang diupload ke variabel $file
        $file = $request->file('gambar');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // arah upload
        $tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);

        Item::create([
            'nama' => $request->nama,
            'hargabeli' => $request->hargabeli,
            'hargajual' => $request->hargajual,
            'persediaan' => $request->persediaan,
            'gambar' => $nama_file

        ]);

        return redirect('')->with('sukses', 'Item berhasil ditambahkan !');
    }


    public function store(Request $request)
    {
        // if ($request->file('gambar')) {
        //     $destinationPath = "img/";

        //     if (!is_dir($destinationPath)) {
        //         mkdir($destinationPath, 0777, true);
        //     }
        //     $final_location = $destinationPath . "/";
        //     $request->file('gambar')
        //         ->move($final_location, filename . '.' . $request->file('gambar')
        //             ->getClientOriginalExtension());
        // }
        // $path = $request->file('gambar')->store('img/');

        // DB::table('itemstock')->insert([
        //     'nama' => $request->nama,
        //     'hargabeli' => $request->hargabeli,
        //     'hargajual' => $request->hargajual,
        //     'persediaan' => $request->persediaan,
        //     'gamber' => $path

        // ]);
    }

    public function show($id)
    {
        $item = \App\Item::find($id);
        return view('item', ['item' => $item]);
    }

    public function edit($id)
    {
        $item = \App\Item::find($id);
        return view('edit', ['item' => $item]);
    }

    public function update(Request $request, $id)
    {
        $item = \App\Item::find($id);
        $item->update($request->all());
        return redirect('/')->with('sukses2', 'Data berhasil diedit !');
    }

    public function destroy($id)
    {
        //
    }
}
