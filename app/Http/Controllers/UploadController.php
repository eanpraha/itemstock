<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class UploadController extends Controller
{
    public function upload()
    {
        $gambar = Item::get();
        return view('upload', ['gambar' => $gambar]);
    }

    public function proses_upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|image|mimes:png,jpg|max:100',
        ]);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('gambar');

        $nama_file = $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = '/img/';
        $file->move($tujuan_upload, $nama_file);

        Item::create([
            'file' => $nama_file,
        ]);

        \App\Item::create($request->all());
        return redirect('/')->with('sukses', 'Item berhasil ditambahkan !');
    }
}
