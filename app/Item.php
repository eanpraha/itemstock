<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'itemstock';
    protected $fillable = ['nama', 'hargabeli', 'hargajual', 'persediaan', 'gambar'];
}
