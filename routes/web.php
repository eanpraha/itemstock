<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// about
Route::get('/about', function () {
    return view('about');
});

// list
Route::get('/list', 'ItemsController@list');


//route CRUD
Route::get('/', 'ItemsController@index');

Route::get('/items', 'ItemsController@index');
Route::post('/items/create', 'ItemsController@create');

Route::post('/items', 'ItemsController@store')->name('create');
Route::post('/items', 'ItemsController@create')->name('create');


Route::get('/items/edit/{id}', 'ItemsController@edit');
Route::post('/items/edit/{id}', 'ItemsController@edit');

Route::get('/items/show/{id}', 'ItemsController@show');

Route::get('/items', 'ItemsController@index');
Route::get('/items/cari', 'ItemsController@cari');

Route::get('/items/hapus/{id}', 'ItemsController@hapus');

// Route::get('/upload', 'UploadController@upload');
// Route::post('/upload/proses', 'UploadController@proses_upload');
